import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage implements OnInit {
	users: any[];
  constructor(public navCtrl: NavController, public service : ServiceProvider, public alertCtrl: AlertController) {

  }
  ngOnInit(){
  		this.getDados();
  	}
  getDados(){
   	this.service.getData()
   		.subscribe(
	   		data => this.users = data,
	   		err => console.log(err),

   		);
   }
  
}
