import { Component, OnInit } from '@angular/core';

import { Validators, FormBuilder } from '@angular/forms';
//import { NgForm } from '@angular/forms';

import { NavController, AlertController, ToastController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})

export class ContactPage implements OnInit {
	users : any[];
	cadastro : any = {};

	constructor(public navCtrl: NavController,
				public service : ServiceProvider,
				public formBuilder: FormBuilder,
				public alertCtrl: AlertController,
				public toastCtrl: ToastController) {
		this.getDados();
		this.cadastro = this.formBuilder.group({
			nome:['', Validators.required],
			email:['', Validators.required],
      profissao:['', Validators.required],
			cod:['', Validators.required],
			telefone:['', Validators.required]


		});

  }
  	ngOnInit(){
  		this.getDados();
  	}


  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      this.getDados();	
      console.log('Async operation has ended');
      refresher.complete();
    }, 1000);
  }
  getDados(){

   	this.service.getData()
   		.subscribe(
	   		data => this.users = data,
	   		err => console.log(err)
   		);

   }
    postDados(){
   		console.log(this.cadastro.value);

   		this.service.postData(this.cadastro.value)
   			.subscribe(
   				data => {
   						console.log(data.mensage)
   						this.getDados();
   						},
   				err => console.log(err)
   			);
   		let toast = this.toastCtrl.create({
	      message: 'Usuário cadastrado com sucesso',
	      duration: 2000
	    });
	    toast.present();

   }

}
