import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, ActionSheetController, ToastController } from 'ionic-angular';
import { Http, Headers, Response, ResponseOptions } from '@angular/http';

import { ServiceProvider } from '../../providers/service/service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'


})
export class HomePage {

  searchQuery: string = '';
  users: any='';


  constructor(public navCtrl: NavController,
  			  public service : ServiceProvider,
  			  public alertCtrl: AlertController,
  			  public actionSheetCtrl: ActionSheetController,
          public http: Http,
  			  public toastCtrl: ToastController) {


  }
  ngOnInit(){
    this.getDados();
    this.initializeItems();
  }

  initializeItems() {
    /*this.service.getData()
       .subscribe(
       data => this.users = data,
       err => console.log(err),

    );*/
    this.http.get('http://filmon.16mb.com/apiRecupera.php').map(res => res.json()).subscribe(data => {
        this.users = data;
    });

 }
onCancel() {
  
  this.initializeItems();
}


 getItems(ev: any) {
   // Reset items back to all of the items




   // set val to the value of the searchbar
   let val = ev.target.value;

   // if the value is an empty string don't filter the items
   if (val && val.trim() != '') {

     this.users = this.users.filter((item) => {

       if (item.id.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
           item.nome.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
           item.email.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
           item.profissao.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
           item.telefone.toLowerCase().indexOf(val.toLowerCase()) > -1 ){

        return true;
      }else{

        return false;
      }

     })
   }
 }


 getDados(){
  this.service.getData()
    .subscribe(
      data => this.users = data,
      err => console.log(err)

    );
 }

  editarPerfil(req) {

    let prompt = this.alertCtrl.create({
      title: 'Editar Informações',
      message: "Altere Informações dos usuários",
      inputs: [
        {
          name: 'nome',
          placeholder: 'Nome',
          value:req.nome
        },
        {
          name: 'email',
          placeholder: 'Email',
          value:req.email
        },
        {
          name: 'profissao',
          placeholder: 'Profissão',
          value:req.profissao
        },
        {
          name: 'telefone',
          placeholder: 'Telefone',
          value:req.telefone
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancelado');
          }
        },
        {
          text: 'Atualizar',
          handler: data => {
          	let params : any={
          		id: req.id,
          		nome: data.nome,
          		email: data.email,
          		profissao: data.profissao,
          		telefone: data.telefone

          	}
            this.service.updateData(params)
   			.subscribe(
   				data => {
   						console.log(data.mensage)
   						this.getDados();
   						},
   				err => console.log(err)
   			);
          }
        }
      ]
    });
    prompt.present();
  }

	presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Informações',
      buttons: [
        {
          text: 'Destructive',
          role: 'destructive',
          handler: () => {
            console.log('Destructive clicked');
          }
        },{
          text: 'Archive',
          handler: () => {
            console.log('Archive clicked');
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
 	doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      this.getDados();
      console.log('Async operation has ended');
      refresher.complete();
    }, 1000);
  }



 	deletarPerfil(user){

 		console.log(user.id);
   		this.service.deleteData(user.id)
   			.subscribe(
   				data => {
   						console.log(data.mensage);
   						this.getDados();
   						},
   				err => console.log(err)
   			);
   		let toast = this.toastCtrl.create({
	      message: 'Usuário deletado com sucesso',
	      duration: 2000
	    });
	    toast.present();
   }
}
