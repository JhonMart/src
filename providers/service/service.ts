import { Injectable } from '@angular/core';
import { Http, Headers, Response, ResponseOptions } from '@angular/http';
import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Observable';
/*
  Generated class for the ServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServiceProvider {
  
  api: string = "http://filmon.16mb.com/";

  constructor(public http: Http) {

  }
  //Função para recuperar dados
  getData(){
  	return this.http.get(this.api+"apiRecupera.php").map(res => res.json());
  }
  //Função para cadastrar
  postData(parans){
  	let headers = new Headers({ 'Content-Type' : 'application/x-www-form-urlencoded'});
  	return this.http.post(this.api + "apiCadastro.php", parans, {
  		headers:headers,
  		method: "POST"
  	}).map(
  		(res:Response) => {return res.json();}
  	);

  }
  //Função para deletar
  deleteData(id){
  	let headers = new Headers({ 'Content-Type' : 'application/x-www-form-urlencoded'});
  	return this.http.post(this.api + "apiDeleta.php", id, {
  		headers:headers,
  		method: "POST"
  	}).map(
  		(res:Response) => {return res.json();}
  	);

  }
  //Função para atualizar
  updateData(data){
  	let headers = new Headers({ 'Content-Type' : 'application/x-www-form-urlencoded'});
  	return this.http.post(this.api + "apiUpdate.php", data, {
  		headers:headers,
  		method: "POST"
  	}).map(
  		(res:Response) => {return res.json();}
  	);

  }


}
